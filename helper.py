import numpy as np

from model.ffnn import NeuralNetwork, compute_loss
import matplotlib.pyplot as plt


def batch_train(X, Y, model, train_flag=False):
    ################################# STUDENT SOLUTION #############################
    # YOUR CODE HERE
    #     TODO:
    #         1) Use your neural network to predict the intent
    #         (without any training) and calculate the accuracy 
    #         of the classifier. Should you be expecting high
    #         numbers yet?
    #         2) if train_flag is true, run the training for 1000 epochs using 
    #         learning rate = 0.005 and use this neural network to predict the 
    #         intent and calculate the accuracy of the classifier
    #         3) Then, plot the cost function for each iteration and
    #         compare the results after training with results before training
    cost_hist = []
    train_flag = False
    if not train_flag:
        res = model.predict(X)
        cost = compute_loss(res, Y) / Y.shape[0]
        print(f"Cost after training: {np.round(cost, 3)}")
        print(f"Accuracy: {calc_accuracy(res, Y)}")

    lr = 0.005
    for i in range(2000):
        res = model.predict(X)
        # print("res", res.shape)
        # print("Y", Y.shape)
        cost = compute_loss(res, Y) / Y.shape[0]
        cost_hist.append(cost)
        dW1, db1, dW2, db2 = model.backward(X, Y)

        model.W1 -= lr*dW1
        model.W2 -= lr*dW2
        model.b1 -= lr*db1.reshape(-1, 1)
        model.b2 -= lr*db2.reshape(-1, 1)
        if i % 50 == 0:
            print(i, cost)

    print("finished training!")
    res = model.predict(X)
    cost = compute_loss(res, Y) / Y.shape[0]
    cost_hist.append(cost)
    # print(cost_hist)
    print(f"Cost without training: {np.round(cost, 3)}")
    print(f"Accuracy: {calc_accuracy(res, Y)}")
    plt.plot(cost_hist)
    plt.show()


def minibatch_train(X, Y, model, train_flag=False, batch_size=64):
    cost_hist = []

    if not train_flag:
        choice = np.random.choice(np.array(range(X.shape[0])), batch_size)
        X_batch = X[choice]
        Y_batch = Y[choice]
        res = model.predict(X_batch)
        cost = compute_loss(res, Y_batch) / Y_batch.shape[0]
        print(f"Cost without training: {np.round(cost, 3)}")
        print(f"Accuracy: {calc_accuracy(res, Y_batch)}")

    lr = 0.005

    for i in range(1000):
        choice = np.random.choice(np.array(range(X.shape[0])), batch_size)
        X_batch = X[choice]
        Y_batch = Y[:, choice]

        res = model.predict(X_batch)
        cost = compute_loss(res, Y_batch) / Y_batch.shape[0]
        cost_hist.append(cost)
        dW1, db1, dW2, db2 = model.backward(X_batch, Y_batch)
        model.W1 -= lr * dW1
        model.W2 -= lr * dW2
        model.b1 -= lr * db1.reshape(-1, 1)
        model.b2 -= lr * db2.reshape(-1, 1)

    res = model.predict(X_batch)
    cost = compute_loss(Y_batch, res) / Y_batch.shape[0]
    cost_hist.append(cost)
    plt.plot(cost_hist)
    plt.show()
    res = model.predict(X)
    print(f"Accuracy: {calc_accuracy(res, Y)}")
    plt.plot(cost_hist)
    plt.show()

def calc_accuracy(y_true, y_predicted):
    return np.sum(np.argmax(y_true, axis=0) == np.argmax(y_predicted, axis=0)) / y_true.shape[1]