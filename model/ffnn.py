import numpy as np
import numpy.typing as npt

from model.model_utils import softmax, softmax_prime, relu, relu_prime
from typing import Tuple


class NeuralNetwork(object):
    def __init__(
        self, 
        input_size: int,
        hidden_size: int, 
        num_classes: int,
        seed: int = 1
    ):
        """
        Initialize neural network's weights and biases.
        """
        np.random.seed(seed)
        self.W1 = np.random.uniform(low=-1, high=1, size=(hidden_size, input_size))
        self.W2 = np.random.uniform(low=-1, high=1, size=(num_classes, hidden_size))
        self.b1 = np.random.uniform(low=-1, high=1, size=(hidden_size, 1))
        self.b2 = np.random.uniform(low=-1, high=1, size=(num_classes, 1))

    def forward(self, X: npt.ArrayLike) -> npt.ArrayLike:
        """
        Forward pass with X as input matrix, returning the model prediction
        Y_hat.
        """

        z0 = self.W1.dot(X.T) + self.b1
        z1 = relu(z0)
        z2 = self.W2.dot(z1) + self.b2
        z3 = softmax(z2)
        return {"z0": z0, "z1": z1, "z2": z2, "z3": z3}

    def predict(self, X: npt.ArrayLike) -> npt.ArrayLike:
        """
        Create a prediction matrix with `self.forward()`
        """
        ######################### STUDENT SOLUTION ###########################
        # YOUR CODE HERE
        #     TODO:
        #         1) Create a prediction matrix of the intent data using
        #         `self.forward()` function. The shape of prediction matrix
        #         should be similar to label matrix produced with
        #         `labels_matrix()`
        ######################################################################

        z0 = self.W1.dot(X.T) + self.b1
        z1 = relu(z0)
        z2 = self.W2.dot(z1) + self.b2
        z3 = softmax(z2)
        return z3

    def backward(
        self, 
        X: npt.ArrayLike, 
        Y: npt.ArrayLike
    ) -> Tuple[npt.ArrayLike, npt.ArrayLike, npt.ArrayLike, npt.ArrayLike]:
        """

        Backpropagation algorithm.
        """
        preds_all = self.forward(X)
        z0 = preds_all["z0"]
        z1 = preds_all["z1"]
        z2 = preds_all["z2"]
        z3 = preds_all["z3"]

        # second layer
        err2 = z3 - Y # softmax prime
        n = X.shape[0]
        dW2 = err2.dot(z1.T) / n
        db2 = np.mean(err2, axis=1)


        err1 = self.W2.T.dot(err2) * relu_prime(z0)
        dW1 = err1.dot(X) / n #np.dot(X.T, err1) / n
        db1 = np.mean(err1, axis=1)
        return dW1, db1, dW2, db2


def compute_loss(pred: npt.ArrayLike, truth: npt.ArrayLike) -> float:
    """
    Compute the cross entropy loss.
    """
    return -np.sum(truth*np.log(pred)) / truth.shape[0]


def compute_loss_grad(pred: npt.ArrayLike, truth: npt.ArrayLike) -> float:
    """
    Compute the cross entropy loss.
    """

    return -truth/(pred + 1e-10)
