import numpy as np
import numpy.typing as npt

from typing import Tuple, List, Set


def bag_of_words_matrix(sentences: List[str]) -> npt.ArrayLike:
    """
    Convert the dataset into V x M matrix.
    """

    i = 0
    d = {}
    for sentence in sentences:
        for word in sentence.split():
            if word not in d:
                d[word] = i
                i += 1
    X = np.zeros((len(sentences), i))
    for i, sentence in enumerate(sentences):
        for word in sentence.split():
            X[i, d[word]] += 1
    return X


def labels_matrix(data: Tuple[List[str], Set[str]]) -> npt.ArrayLike:
    """
    Convert the dataset into K x M matrix.
    """
    d = {}

    for i, label in enumerate(data[1]):
        d[label] = i
    k = max(list(d.values()))
    Y = np.zeros((len(data[0]), k+1))

    for i, label in enumerate(data[0]):
        Y[i, d[label]] += 1
    return Y.T


def softmax(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    Softmax function.
    """
    A = np.exp(z) / sum(np.exp(z))
    return A

    # tmp = z - z.max(axis=1).reshape(-1, 1)
    # exp_tmp = np.exp(tmp)
    # return exp_tmp / exp_tmp.sum(axis=1).reshape(-1, 1)


def softmax_prime(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    First derivative of softmax function.
    """
    I = np.eye(z.shape[0])
    return softmax(z) * (I - softmax(z).T)


def sigmoid(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    Sigmoid function.
    """
    return 1 / (1 + np.exp(-z))


def sigmoid_prime(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    First derivative of sigmoid function.
    """
    return sigmoid(z) * (1 - sigmoid(z))


def relu(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    Rectified Linear Unit function.
    """
    return z * (z > 0)


def relu_prime(z: npt.ArrayLike) -> npt.ArrayLike:
    """
    First derivative of ReLU function.
    """
    return 1. * (z > 0)
